package com.hellosaurus

import android.app.Application
import com.hellosaurus.core.di.coreModule
import com.hellosaurus.tooling.plantTreesForTimber
import com.pandulapeter.beagle.Beagle
import com.pandulapeter.beagle.common.configuration.Behavior
import com.pandulapeter.beagle.common.configuration.toText
import com.pandulapeter.beagle.modules.*
import org.koin.core.context.startKoin

@Suppress("unused")
class HellosaurusApp : Application() {

    override fun onCreate() {
        super.onCreate()
        setupTooling()
        setupKoin()
    }

    private fun setupTooling() {
        // TODO Crashlytics is disabled from Manifest, enable it if user consented
        plantTreesForTimber()
        initializeBeagle()
    }

    private fun setupKoin() {
        startKoin {
            modules(coreModule)
        }
    }

    private fun initializeBeagle() {
        Beagle.initialize(
            application = this,
            behavior = Behavior(
                shakeDetectionBehavior = Behavior.ShakeDetectionBehavior(threshold = null),
                bugReportingBehavior = Behavior.BugReportingBehavior(
                    logRestoreLimit = 20, // By default this is 20. Decrement the value if the crash reporter feature does not work (!!! FAILED BINDER TRANSACTION !!! error - transaction too large). This is needed if the app has huge logs / network logs.
                    buildInformation = {
                        listOf(
                            "Version name".toText() to BuildConfig.VERSION_NAME,
                            "Version code".toText() to BuildConfig.VERSION_CODE.toString(),
                            "Application ID".toText() to BuildConfig.APPLICATION_ID
                        )
                    }
                )
            )
        )
        Beagle.set(
            HeaderModule(
                title = getString(R.string.app_name),
                subtitle = BuildConfig.APPLICATION_ID,
                text = "${BuildConfig.BUILD_TYPE} v${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})"
            ),
            AppInfoButtonModule(),
            DividerModule(),
            DeveloperOptionsButtonModule(),
            PaddingModule(),
            TextModule("General", TextModule.Type.SECTION_HEADER),
            KeylineOverlaySwitchModule(),
            AnimationDurationSwitchModule(),
            ScreenCaptureToolboxModule(),
            DividerModule(),
            TextModule("Logs", TextModule.Type.SECTION_HEADER),
            NetworkLogListModule(),
            LogListModule(), // Use Beagle.log() or BeagleLogger.log() to push messages
            LifecycleLogListModule(),
            DividerModule(),
            TextModule("Other", TextModule.Type.SECTION_HEADER),
            DeviceInfoModule(),
            ForceCrashButtonModule()
        )
    }
}
