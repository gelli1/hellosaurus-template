package com.hellosaurus.tooling

import android.util.Log
import com.hellosaurus.BuildConfig
import timber.log.Timber


fun plantTreesForTimber() {
    if (BuildConfig.BUILD_TYPE == "debug") {
        Timber.plant(Timber.DebugTree())
    } else {
        Timber.plant(CrashlyticsTree())
    }
}

internal class CrashlyticsTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return
        }
    }
}

// TODO When Beagle is setup plant this tree
// internal class BeagleTree : Timber.Tree() {
//     override fun log(priority: Int, tag: String?, message: String, t: Throwable?) =
//         Beagle.log("[$tag] $message", "Timber", t?.stackTraceToString())
// }