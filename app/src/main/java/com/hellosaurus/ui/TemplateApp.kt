package com.hellosaurus.ui

import androidx.compose.runtime.Composable
import com.google.accompanist.insets.ProvideWindowInsets
import com.hellosaurus.ui.screen.AppNavigation
import com.hellosaurus.ui.theme.TemplateTheme

@Composable
fun TemplateApp() {
    TemplateTheme {
        ProvideWindowInsets {
            AppContent()
        }
    }
}

@Composable
fun AppContent() {
    AppNavigation()
}