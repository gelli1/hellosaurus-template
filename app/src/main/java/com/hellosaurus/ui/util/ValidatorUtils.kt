package com.hellosaurus.ui.util

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ValidatorState(
    val textFieldValue: String ="",
    val isValid: Boolean = true
):Parcelable

