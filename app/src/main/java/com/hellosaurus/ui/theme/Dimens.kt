package com.hellosaurus.ui.theme

import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

internal val LocalDimens = staticCompositionLocalOf { Dimens() }

val appDimens = Dimens()

data class Dimens(
    val screenPadding: Dp = 16.dp,
    val smallPadding: Dp = 4.dp,
    val itemPadding: Dp = 8.dp,
    val buttonPadding: Dp = 8.dp,
    val buttonSize: Dp = 56.dp,
)