package com.hellosaurus.ui.theme

import androidx.compose.foundation.LocalIndication
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.ReadOnlyComposable

object AppTheme {
    val dimens: Dimens
        @Composable
        @ReadOnlyComposable
        get() = LocalDimens.current

    val colors: Colors
        @Composable
        @ReadOnlyComposable
        get() = LocalColors.current

    val typography: Typography
        @Composable
        @ReadOnlyComposable
        get() = LocalTypography.current

    val shapes: Shapes
        @Composable
        @ReadOnlyComposable
        get() = LocalShapes.current
}

@Composable
fun TemplateTheme(content: @Composable () -> Unit) {
    val rippleIndication = rememberRipple()
    CompositionLocalProvider(
        LocalColors provides appColors,
        LocalDimens provides appDimens,
        LocalTypography provides appTypography,
        LocalShapes provides appShapes,
        LocalIndication provides rippleIndication,
        content = content
    )
}