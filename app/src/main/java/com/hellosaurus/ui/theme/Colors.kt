package com.hellosaurus.ui.theme

import androidx.compose.runtime.Immutable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color

internal val LocalColors = staticCompositionLocalOf { appColors }

val appColors: Colors = Colors()

@Immutable
data class Colors(
    val accent: Color = Color(0xFF5A3E75),
    val accentVariant: Color = Color(0xFF8C1AFF),
    val surface: Color = Color(0xFFFBF1CC),
    val surfaceVariant: Color = Color(0x080D1C2E),
    val textOnSurface: Color = Color(0xFF0D1C2E),
    val textOnSurfaceLight: Color = Color(0xA30D1C2E),
    val textOnAccent: Color = Color(0xFFFFFFFF),
)