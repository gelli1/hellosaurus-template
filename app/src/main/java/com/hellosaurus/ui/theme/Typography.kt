package com.hellosaurus.ui.theme

import androidx.compose.runtime.Immutable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.hellosaurus.R

val LocalTypography = staticCompositionLocalOf { appTypography }

val appTypography = Typography(appColors)

val merriweatherFamily = FontFamily(
    fonts = listOf(
        Font(resId = R.font.merriweather,
            weight = FontWeight.Normal,
            style = FontStyle.Normal),
    )
)

val workSansFamily = FontFamily(
    fonts = listOf(
        Font(resId = R.font.work_sans,
            weight = FontWeight.Normal,
            style = FontStyle.Normal),
    )
)

@Immutable
data class Typography(
    val colors: Colors,
    val title: TextStyle = TextStyle(colors.textOnSurface,
        32.sp,
        lineHeight = 32.sp,
        fontFamily = merriweatherFamily,
        fontWeight = FontWeight.Bold,
        fontStyle = FontStyle.Normal),
    val subtitle: TextStyle = TextStyle(colors.textOnSurface,
        18.sp,
        lineHeight = 20.sp,
        fontFamily = merriweatherFamily,
        fontWeight = FontWeight.Normal,
        fontStyle = FontStyle.Normal),
    val body: TextStyle = TextStyle(colors.textOnSurface,
        16.sp,
        lineHeight = 24.sp,
        fontFamily = workSansFamily,
        fontWeight = FontWeight.Normal,
        fontStyle = FontStyle.Normal),
    val button: TextStyle = TextStyle(colors.textOnSurfaceLight,
        16.sp,
        lineHeight = 24.sp,
        fontFamily = workSansFamily,
        fontWeight = FontWeight.Normal,
        fontStyle = FontStyle.Normal),
)