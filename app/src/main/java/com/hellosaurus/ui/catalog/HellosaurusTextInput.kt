package com.hellosaurus.ui.catalog

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color.Companion.Red
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.hellosaurus.ui.theme.AppTheme

@Composable
fun HellosaurusInput(
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    labelText: @Composable () -> Unit,
    errorText: String?
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .clip(AppTheme.shapes.small)
            .background(AppTheme.colors.accent)
            .padding(bottom = 4.dp)
            .border(border = BorderStroke(4.dp, AppTheme.colors.accent), shape = AppTheme.shapes.small)
            .clip(AppTheme.shapes.small)
            .background(White)
            .animateContentSize()
    ) {
        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = value,
            onValueChange = onValueChange,
            label = labelText,
            visualTransformation = visualTransformation,
            colors = setTextFieldColors(),
            singleLine = true,
        )
        AnimatedVisibility(errorText != null) {
            Text(
                text = errorText.orEmpty(), modifier = Modifier
                    .fillMaxSize()
                    .offset(y = (-8).dp)
                    .padding(start = 16.dp, bottom = 4.dp), color = Red
            )
        }
    }
}

@Composable
private fun setTextFieldColors() = TextFieldDefaults.textFieldColors(
    backgroundColor = White,
    focusedIndicatorColor = Transparent,
    unfocusedIndicatorColor = Transparent,
    disabledIndicatorColor = Transparent,
    cursorColor = AppTheme.colors.accent,
)