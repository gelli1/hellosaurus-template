package com.hellosaurus.ui.catalog

import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import com.hellosaurus.ui.theme.AppTheme

@Composable
fun LoadingIndicator() {
    CircularProgressIndicator(color = AppTheme.colors.accent)
}