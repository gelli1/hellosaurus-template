package com.hellosaurus.ui.catalog

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.Saver
import androidx.compose.runtime.saveable.listSaver
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.End
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import com.hellosaurus.ui.screen.home.UiItemPreview
import com.hellosaurus.ui.theme.AppTheme

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun ItemPreview(
    item: UiItemPreview,
    modifier: Modifier = Modifier,
    itemPreviewState: ItemPreviewState = rememberItemPreviewState(),
    onClick: () -> Unit,
) {
    Box(modifier = modifier
        .clip(AppTheme.shapes.small)
        .clickable { itemPreviewState.expanded = !itemPreviewState.expanded }
        .padding(AppTheme.dimens.itemPadding)) {

        Column(modifier = Modifier.fillMaxWidth()) {
            Title(text = item.title)
            AnimatedVisibility(itemPreviewState.expanded) {
                ItemDetail(item = item, onClick = onClick)
            }
        }
    }
}

@Composable
private fun ItemDetail(
    item: UiItemPreview,
    onClick: () -> Unit,
) {
    Column(modifier = Modifier.fillMaxWidth()) {
        Subtitle(text = item.subtitle)
//        Button(text = "open", onClick = onClick, Modifier.align(End))
    }
}

@Composable
fun rememberItemPreviewState(expanded: Boolean = false): ItemPreviewState = rememberSaveable(saver = ItemPreviewState.Saver) { ItemPreviewState(expanded) }

class ItemPreviewState(expanded: Boolean = false) {
    var expanded by mutableStateOf(expanded)

    companion object {
        val Saver: Saver<ItemPreviewState, *> = listSaver(
            save = { listOf(it.expanded) },
            restore = { ItemPreviewState(it[0]) }
        )
    }
}