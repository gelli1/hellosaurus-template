package com.hellosaurus.ui.catalog

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.hellosaurus.R
import com.hellosaurus.ui.theme.AppTheme

@Composable
fun GreetingItem(modifier: Modifier = Modifier, titleText: String, subtitleText: String) {
    Row(modifier = modifier) {
        Column(
            modifier = Modifier
                .weight(1f)
                .padding(top = AppTheme.dimens.buttonSize)
        ) {
            Title(text = titleText, modifier = Modifier.padding(top = AppTheme.dimens.screenPadding))
            Subtitle(
                text = subtitleText,
                modifier = Modifier.padding(top = AppTheme.dimens.itemPadding),
                color = AppTheme.colors.accent
            )
        }
        Box(modifier = Modifier.align(Alignment.Bottom)) {
            Image(
                contentScale = ContentScale.Crop,
                modifier = Modifier.requiredWidth(84.dp),
                painter = painterResource(R.mipmap.img_dino),
                contentDescription = null,
            )
        }
    }
}
