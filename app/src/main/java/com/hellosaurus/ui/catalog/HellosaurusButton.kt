package com.hellosaurus.ui.catalog

import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.hellosaurus.ui.theme.AppTheme

@Composable
fun HellosaurusButton(modifier: Modifier = Modifier, onClick: () -> Unit, content: @Composable BoxScope.() -> Unit) {
    var isPressed by remember { mutableStateOf(false) }
    val offset: Dp by animateDpAsState(targetValue = if (isPressed) 4.dp else 0.dp, animationSpec = tween(durationMillis = 100, easing = FastOutSlowInEasing))
    Box(
        modifier = modifier
            .height(AppTheme.dimens.buttonSize)
            .pointerInput(Unit) {
                detectTapGestures(
                    onPress = {
                        isPressed = true
                        awaitRelease()
                        onClick()
                        isPressed = false
                    }
                )
            }
    ) {
        Spacer(
            modifier = Modifier
                .fillMaxSize()
                .offset(y = 6.dp)
                .background(color = AppTheme.colors.accent, AppTheme.shapes.small)
        )

        Box(
            modifier = Modifier
                .height(AppTheme.dimens.buttonSize)
                .offset(y = offset)
                .fillMaxWidth()
                .clip(AppTheme.shapes.small)
                .background(AppTheme.colors.accentVariant)
        ) {
            content()
        }
    }
}