package com.hellosaurus.ui.catalog

import androidx.compose.foundation.text.BasicText
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import com.hellosaurus.ui.theme.AppTheme

@Composable
fun Title(text: String, modifier: Modifier = Modifier) {
    Text(text = text, modifier = modifier, style = AppTheme.typography.title, color = AppTheme.colors.accent)
}

@Composable
fun Subtitle(text: String, modifier: Modifier = Modifier, color: Color = AppTheme.colors.textOnSurface) {
    Text(
        text,
        style = AppTheme.typography.subtitle,
        modifier = modifier,
        color = color
    )
}

@Composable
fun Body(text: String, modifier: Modifier = Modifier) {
    BasicText(
        text,
        style = AppTheme.typography.body,
        modifier = modifier
    )
}

@Composable
fun HellosaurusTextButton(modifier: Modifier = Modifier, text: String, onClick: () -> Unit) {
    HellosaurusButton(modifier = modifier, onClick = onClick) {
        Text(
            modifier = Modifier.align(Alignment.Center),
            text = text,
            style = AppTheme.typography.body.copy(textAlign = TextAlign.Center),
            color = Color.White
        )
    }
}

