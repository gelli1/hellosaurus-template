package com.hellosaurus.ui.catalog

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.hellosaurus.R

@Composable
fun UpButton(modifier: Modifier = Modifier, onClick: () -> Unit) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .clip(CircleShape)
            .clickable {
                println("Called")
                onClick()
            }
            .padding(8.dp)
            .shadow(elevation = 8.dp, shape = CircleShape)
    ) {
        Image(
            painter = painterResource(R.drawable.ic_bg_up_button),
            contentDescription = "Contact profile picture",
        )
        Image(
            painter = painterResource(R.drawable.ic_back),
            contentDescription = "Contact profile picture",
            contentScale = ContentScale.Crop,
        )
    }
}