package com.hellosaurus.ui.screen.login

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.Saver
import androidx.compose.runtime.saveable.listSaver
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import com.hellosaurus.core.usecase.EmailValidator
import com.hellosaurus.core.usecase.PasswordValidator
import com.hellosaurus.ui.util.ValidatorState

@Composable
fun rememberLoginState(): LoginState = rememberSaveable(saver = LoginState.Saver) { LoginState() }

class LoginState(
    passwordState: ValidatorState = ValidatorState(),
    emailState: ValidatorState = ValidatorState()
) {
    private val validateEmail = EmailValidator()
    private val passwordValidator = PasswordValidator()

    var passwordState by mutableStateOf(ValidatorState())
    var emailState by mutableStateOf(ValidatorState())

    init {
        this.passwordState = passwordState
        this.emailState = emailState
    }

    fun login() {
        emailState = emailState.copy(isValid = validateEmail(emailState.textFieldValue))
        passwordState =
            passwordState.copy(isValid = passwordValidator(passwordState.textFieldValue))

        if (emailState.isValid && passwordState.isValid) {
            //TODO call use case to login
        }
    }

    fun onEmailChanged(textFieldValue: String) {
        emailState = emailState.copy(textFieldValue = textFieldValue, isValid = true)
    }

    fun onPasswordChanged(textFieldValue: String) {
        passwordState = passwordState.copy(textFieldValue = textFieldValue, isValid = true)
    }

    sealed class Action {
        object OnLoginClicked : Action()
        data class OnForgotPasswordClicked(val email: String) : Action()
    }

    companion object {
        val Saver: Saver<LoginState, *> = listSaver(
            save = { listOf(it.emailState, it.passwordState) },
            restore = { LoginState(it[1], it[0]) }
        )
    }
}


