package com.hellosaurus.ui.screen.home

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import com.hellosaurus.core.model.ItemPreview
import com.hellosaurus.core.usecase.GetItemsUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

@Composable
fun rememberHomeScreenState(getItems: GetItemsUseCase): HomeScreenState = remember { HomeScreenState(getItems) }

class HomeScreenState(getItems: GetItemsUseCase) {
    val items: Flow<Result> = getItems.invoke().map<List<ItemPreview>, Result> { items ->
        Result.Success(items.map { UiItemPreview(it.id, it.title, it.subtitle, it.favourite) })
    }.onStart { emit(Result.Loading) }
}

data class UiItemPreview(
    val id: String,
    val title: String,
    val subtitle: String,
    val favourite: Boolean,
)

sealed class Result {
    object Loading : Result()
    class Success(val items: List<UiItemPreview>) : Result()
}