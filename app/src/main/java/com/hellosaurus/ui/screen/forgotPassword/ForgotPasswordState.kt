package com.hellosaurus.ui.screen.forgotPassword

import androidx.compose.runtime.*
import androidx.compose.ui.text.input.TextFieldValue
import com.hellosaurus.core.usecase.EmailValidator
import com.hellosaurus.ui.util.ValidatorState

@Composable
fun rememberForgotPasswordState(email: String?): ForgotPasswordState = remember { ForgotPasswordState(email = email) }

class ForgotPasswordState(email: String?) {

    private val validateEmail = EmailValidator()

    var emailState by mutableStateOf(ValidatorState())

    init {
//        email?.let { emailState = emailState.copy(textFieldValue = emailState.textFieldValue.copy(text = email)) }
    }

    fun onEmailChanged(textFieldValue: String) {
//        emailState = emailState.copy(textFieldValue = textFieldValue, isValid = true)
    }

    fun onLoginClicked() {

    }

}
