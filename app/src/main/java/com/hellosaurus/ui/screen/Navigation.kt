package com.hellosaurus.ui.screen

import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.hellosaurus.ui.screen.detail.DetailScreen
import com.hellosaurus.ui.screen.forgotPassword.ForgotPasswordScreen
import com.hellosaurus.ui.screen.home.HomeScreen
import com.hellosaurus.ui.screen.login.LoginScreen
import com.hellosaurus.ui.screen.login.LoginState
import com.hellosaurus.ui.theme.AppTheme

@OptIn(ExperimentalFoundationApi::class, androidx.compose.animation.ExperimentalAnimationApi::class)
@Composable
fun AppNavigation() {
    val navController = rememberAnimatedNavController()
    AnimatedNavHost(navController, startDestination = "login", modifier = Modifier.background(AppTheme.colors.surface)) {
        composable(route = "login") {
            LoginScreen(onItemClick = { action ->
                when (action) {
                    is LoginState.Action.OnForgotPasswordClicked -> navController.navigate("forgotPassword?email=${action.email}")
                    LoginState.Action.OnLoginClicked -> TODO()
                }
            })
        }

        composable(
            route = "forgotPassword?email={email}",
            enterTransition = { slideIntoContainer(AnimatedContentScope.SlideDirection.Left) },
            exitTransition = { fadeOut() },
            arguments = listOf(navArgument("email") {
                defaultValue = null
                nullable = true
                type = NavType.StringType
            })
        ) { backStackEntry ->
            ForgotPasswordScreen(email = backStackEntry.arguments?.getString("email")) { navController.navigateUp() }
        }

        composable("home") { HomeScreen(onItemClick = { navController.navigate("detail/$it") }) }
        composable("detail/{itemId}") { backStackEntry ->
            backStackEntry.arguments?.getString("itemId")?.let { itemId ->
                DetailScreen(itemId)
            } ?: navController.popBackStack()
        }
    }
}