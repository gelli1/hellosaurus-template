package com.hellosaurus.ui.screen.forgotPassword

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.google.accompanist.insets.navigationBarsPadding
import com.google.accompanist.insets.statusBarsPadding
import com.google.accompanist.insets.systemBarsPadding
import com.hellosaurus.R
import com.hellosaurus.ui.catalog.GreetingItem
import com.hellosaurus.ui.catalog.HellosaurusInput
import com.hellosaurus.ui.catalog.HellosaurusTextButton
import com.hellosaurus.ui.catalog.UpButton
import com.hellosaurus.ui.theme.AppTheme

@ExperimentalFoundationApi
@Composable
fun ForgotPasswordScreen(email: String?, screenState: ForgotPasswordState = rememberForgotPasswordState(email = email), onClick: () -> Unit) {
    Box {
        LazyColumn(
            contentPadding = PaddingValues(AppTheme.dimens.screenPadding),
            modifier = Modifier
                .systemBarsPadding(top = false, bottom = false)
                .fillMaxSize()
                .background(AppTheme.colors.surface)
        ) {
            item { Spacer(modifier = Modifier.statusBarsPadding()) }
            item {
                Box(Modifier.animateItemPlacement()) {
                    GreetingItem(
                        titleText = stringResource(R.string.forgot_password_title),
                        subtitleText = stringResource(R.string.forgot_password_description)
                    )
                }
            }
            item { Box(Modifier.animateItemPlacement()) { EmailInput(screenState = screenState) } }
            item { Box(Modifier.animateItemPlacement()) { LoginButton { screenState.onLoginClicked() } } }
            item { Spacer(modifier = Modifier.navigationBarsPadding()) }
        }
        UpButton(modifier = Modifier.systemBarsPadding()) { onClick() }
    }
}

@Composable
private fun EmailInput(screenState: ForgotPasswordState) {
    HellosaurusInput(
        labelText = { Text(stringResource(R.string.enter_email)) },
        onValueChange = { screenState.onEmailChanged(it) },
        value = screenState.emailState.textFieldValue,
        modifier = Modifier.padding(top = AppTheme.dimens.screenPadding),
        errorText = if (!screenState.emailState.isValid) stringResource(id = R.string.email_error) else null
    )
}

@Composable
private fun LoginButton(onClick: () -> Unit) {
    HellosaurusTextButton(modifier = Modifier.padding(top = AppTheme.dimens.screenPadding), text = stringResource(id = R.string.submit), onClick = onClick)
}
