package com.hellosaurus.ui.screen.detail

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.text.BasicText
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import com.google.accompanist.insets.systemBarsPadding
import com.hellosaurus.core.model.Item
import com.hellosaurus.ui.catalog.LoadingIndicator
import org.koin.androidx.compose.get

@Composable
fun DetailScreen(
    itemId: String,
    detailScreenState: DetailScreenState = rememberDetailScreenState(itemId, get())
) {
    val itemDetails by detailScreenState.itemDetails.collectAsState(initial = Result.Loading)

    when (val result = itemDetails) {
        Result.Loading -> LoadingDetails()
        is Result.Success -> DetailsScreenContent(result.item)
        Result.Error -> {}
    }
}

@Composable
fun LoadingDetails() {
    Box(Modifier.systemBarsPadding()) {
        LoadingIndicator()
    }
}

@Composable
fun DetailsScreenContent(item: Item) {
    Box(Modifier.systemBarsPadding()) {
        BasicText(text = "$item")
    }
}

