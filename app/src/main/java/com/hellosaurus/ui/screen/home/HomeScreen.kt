package com.hellosaurus.ui.screen.home

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.google.accompanist.insets.navigationBarsPadding
import com.google.accompanist.insets.statusBarsPadding
import com.google.accompanist.insets.systemBarsPadding
import com.hellosaurus.ui.catalog.ItemPreview
import com.hellosaurus.ui.catalog.LoadingIndicator
import com.hellosaurus.ui.theme.AppTheme
import org.koin.androidx.compose.get

@Composable
fun HomeScreen(
    screenState: HomeScreenState = rememberHomeScreenState(get()),
    onItemClick: (itemId: String) -> Unit
) {
    val homeItemsResult by screenState.items.collectAsState(Result.Loading)

    when (homeItemsResult) {
        Result.Loading -> HomeScreenLoading()
        is Result.Success -> HomeScreenItems((homeItemsResult as Result.Success).items, onItemClick = { onItemClick(it.id) })
    }
}

@Composable
private fun HomeScreenLoading() {
    Box(
        Modifier
            .systemBarsPadding()
            .fillMaxSize(), contentAlignment = Alignment.Center
    ) {
        LoadingIndicator()
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun HomeScreenItems(
    listItems: List<UiItemPreview>,
    onItemClick: (item: UiItemPreview) -> Unit,
) {
    LazyColumn(Modifier.systemBarsPadding(top = false, bottom = false), contentPadding = PaddingValues(AppTheme.dimens.screenPadding)) {
        item { Spacer(modifier = Modifier.statusBarsPadding()) }
        items(listItems, key = { it.id }) { item -> ItemPreview(item, Modifier.animateItemPlacement(), onClick = { onItemClick(item) }) }
        item { Spacer(modifier = Modifier.navigationBarsPadding()) }
    }
}