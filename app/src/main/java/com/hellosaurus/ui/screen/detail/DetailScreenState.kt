package com.hellosaurus.ui.screen.detail

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import com.hellosaurus.core.model.Item
import com.hellosaurus.core.usecase.GetItemUseCase
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart


@Composable
fun rememberDetailScreenState(itemId: String, getItem: GetItemUseCase): DetailScreenState = remember { DetailScreenState(itemId, getItem) }

class DetailScreenState(itemId: String, getItem: GetItemUseCase) {
    val itemDetails = getItem.invoke(itemId).map { if (it == null) Result.Error else Result.Success(it) }
        .onStart { emit(Result.Loading) }
}

sealed class Result {
    object Loading : Result()
    object Error : Result()
    class Success(val item: Item) : Result()
}