plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-parcelize")

    id("com.google.gms.google-services")
}
val internalRelease = "internal"
android {
    compileSdk = 31
    buildToolsVersion = "30.0.3"

    defaultConfig {
        applicationId = "com.hellosaurus"
        minSdk = 23
        targetSdk = 31
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    signingConfigs {
        create(internalRelease) {
            keyAlias = "androiddebugkey"
            keyPassword = "android"
            storeFile = file("/internal.keystore")
            storePassword = "android"
        }
    }

    buildTypes {
        debug {
        }

        create(internalRelease) {
            signingConfig = signingConfigs.getByName(internalRelease)
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }

        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
        // To suppress kotlinx-coroutines-test experimental apis warnings
        freeCompilerArgs = listOf("-Xuse-experimental=kotlinx.coroutines.ExperimentalCoroutinesApi")
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = libs.versions.androidx.compose.get()
    }
}

dependencies {
    // Kotlin
    implementation(libs.kotlinx.coroutines)

    // AndroidX
    implementation(libs.bundles.androidx.core)
    implementation(libs.andoridx.activityCompose)
    implementation(libs.androidx.appcompat)

    implementation(libs.androidx.navigationCompose)
    implementation(libs.bundles.androidx.room)
    kapt(libs.androidx.roomCompiler)

    implementation(libs.bundles.androidx.compose)

    // Google
    implementation(libs.google.android.material)

    // Halcyon
    implementation(project(":core"))
    implementation(libs.halcyon.oauthMoshi)
    implementation(libs.halcyon.errorHandler)
    implementation(libs.halcyon.recylerTypedAdapter)

    // Other Libraries
    implementation(libs.koin.compose)
    implementation(libs.coil)
    releaseImplementation(libs.beagle.noop)
    implementation(libs.square.moshi)
    kapt(libs.square.moshiKotlinCodegen)
    implementation(libs.square.okhttp.loggingInterceptor)
    implementation(libs.square.retrofit)
    implementation(libs.timber)

    // Debug libraries
    debugImplementation(libs.beagle.uiDrawer)
    debugImplementation(libs.leakcanary)
    internalImplementation(libs.beagle.uiDrawer)
    releaseImplementation(libs.beagle.noop)

    // Test libraries
    testImplementation(libs.test.junit4)
    testImplementation(libs.bundles.test.junit5)
    testRuntimeOnly(libs.test.junit5.jupiterEngine)
    // Kotlin
    testImplementation(libs.test.kotlin.coroutines)
    // AndroidX
    testImplementation(libs.test.androidx.room)
    // Robolectric
    testImplementation(libs.bundles.test.robolectric)
    testRuntimeOnly(libs.test.junit5.vintageEngine)
    // Other libraries
    testImplementation(libs.test.mockitoKotlin)
    testImplementation(libs.test.koin)
    testImplementation(libs.test.jraska.livedata)
    testImplementation(libs.test.jakewharton.okhttp3IdlingResource)
    testImplementation(libs.test.square.okhttp.webserver)

    // Android Test libraries
    androidTestImplementation(libs.bundles.test.android)
    androidTestRuntimeOnly(libs.test.junit5.vintageEngine)
    // Kotlin
    androidTestImplementation(libs.test.kotlin.coroutines)
    // Other libraries
    androidTestImplementation(libs.test.jakewharton.okhttp3IdlingResource)
    androidTestImplementation(libs.test.koin)
}

fun DependencyHandler.internalImplementation(dependencyNotation: Any): Dependency? =
    add("internalImplementation", dependencyNotation)
