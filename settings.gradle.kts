enableFeaturePreview("VERSION_CATALOGS") // https://docs.gradle.org/current/userguide/platforms.html

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        maven {
            url = uri("https://maven.pkg.github.com/halcyonmobile/*")
            credentials {
                username = extra.properties["GITHUB_USERNAME"] as String? ?: System.getenv("GITHUB_USERNAME")
                password = extra.properties["GITHUB_TOKEN"] as String? ?: System.getenv("GITHUB_TOKEN")
            }
            // https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token
        }
        google() // https://maven.google.com/web/index.html
        mavenCentral()
        maven { url = uri("https://jitpack.io") }
    }
}
rootProject.name = "ProjectTemplate"
include(":app")
include(":core")
