package com.hellosaurus.core.di

import com.hellosaurus.core.repository.ItemsRepo
import com.hellosaurus.core.usecase.GetItemUseCase
import com.hellosaurus.core.usecase.GetItemsUseCase
import org.koin.dsl.module

val coreModule = module {
    factory { GetItemUseCase(get()) }
    factory { GetItemsUseCase(get()) }
    single { ItemsRepo() }
}

