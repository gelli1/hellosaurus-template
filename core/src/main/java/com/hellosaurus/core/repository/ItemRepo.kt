package com.hellosaurus.core.repository

import com.hellosaurus.core.model.Item
import com.hellosaurus.core.model.Items
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class ItemsRepo {
    private var _items = MutableStateFlow(Items)
    val items: Flow<List<Item>> = _items

    init {
        GlobalScope.launch {
            delay(5000)
            _items.value = _items.value.toMutableList().apply {
                this[6] = this[6].copy(title = "Changed")
                add(2, Item("100", "asdasd", "asdasd", false, "asd"))
                removeAt(9)
                val aux = this[2]
                this[2] = this[4]
                this[4] = aux
            }
        }
    }
}