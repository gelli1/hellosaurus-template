package com.hellosaurus.core.usecase

import com.hellosaurus.core.model.Item
import com.hellosaurus.core.repository.ItemsRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GetItemUseCase(private val itemsRepo: ItemsRepo) {
    operator fun invoke(id: String): Flow<Item?> = itemsRepo.items.map { it.find { it.id == id } }
}
