package com.hellosaurus.core.usecase

import com.hellosaurus.core.model.ItemPreview
import com.hellosaurus.core.repository.ItemsRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.map

class GetItemsUseCase(private val itemsRepo: ItemsRepo) {
    operator fun invoke(): Flow<List<ItemPreview>> = itemsRepo.items.map {
        it.map { ItemPreview(it.id, it.title, it.subtitle, it.favourite) }
    }.debounce(1000)
}