package com.hellosaurus.core.usecase

class PasswordValidator {
    operator fun invoke(password: String) = password.length >= 4
}