package com.hellosaurus.core.model

data class Item(
    val id: String,
    val title: String,
    val subtitle: String,
    val favourite: Boolean,
    val body: String,
)

val Items = (0..50).map { Item("$it", "Title $it", "subtitle $it", false, "body text".repeat(200)) }