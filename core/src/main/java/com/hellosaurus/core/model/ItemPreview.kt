package com.hellosaurus.core.model

data class ItemPreview(
    val id: String,
    val title: String,
    val subtitle: String,
    val favourite: Boolean,
)
