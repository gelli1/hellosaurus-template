[versions]
# Kotlin
kotlin = "1.6.10" # https://kotlinlang.org/docs/releases.html#release-details
kotlinx-coroutines = "1.6.0" # https://github.com/Kotlin/kotlinx.coroutines/releases

# Android tools
androidBuildTools = "7.1.0" # https://developer.android.com/studio/releases/gradle-plugin

# AndroidX libraries
# AndroidX releases: https://developer.android.com/jetpack/androidx/versions#version-table
androidx-core = "1.7.0" # https://developer.android.com/jetpack/androidx/releases/core
androidx-coreSplashscreen = "1.0.0-beta01" # https://developer.android.com/jetpack/androidx/releases/core

androidx-activity = "1.4.0" # https://developer.android.com/jetpack/androidx/releases/activity
androidx-appcompat = "1.3.0" # https://developer.android.com/jetpack/androidx/releases/appcompat
androidx-lifecycle = "2.3.1" # https://developer.android.com/jetpack/androidx/releases/lifecycle

androidx-navigation = "2.4.0" # https://developer.android.com/jetpack/androidx/releases/navigation
androidx-room = "2.4.1" # https://developer.android.com/jetpack/androidx/releases/room

# Compose
androidx-compose = "1.1.0-rc03" # https://developer.android.com/jetpack/androidx/releases/compose
google-accompanist = "0.24.1-alpha" # https://github.com/google/accompanist/releases

# Material Components
google-android-material = "1.5.0" # https://github.com/material-components/material-components-android/releases

# Google libraries for Android
google-services = "4.3.10" # https://developers.google.com/android/guides/google-services-plugin

# Halcyon
halcyon-oauth = '1.0.1' # https://github.com/halcyonmobile/retrofit-oauth2-helper/releases
halcyon-errorHandler = '1.0.0' # https://github.com/halcyonmobile/error-handler/releases
halcyon-recylerTypedAdapter = '1.0.1' # releases page is unmaintained: https://github.com/halcyonmobile/recycler-typed-adapter/releases

# Other libraries
koin = "3.1.5" # https://insert-koin.io/docs/setup/v3
leakcanary = "2.8.1" # https://square.github.io/leakcanary/getting_started/
beagle = "2.6.5" # https://github.com/pandulapeter/beagle/releases
coil = "1.4.0" # https://github.com/coil-kt/coil/releases
square-okhttp = "4.9.1" # https://square.github.io/okhttp/changelog/
square-moshi = "1.13.0" # https://github.com/square/moshi/blob/master/CHANGELOG.md
square-retrofit = "2.9.0" # https://github.com/square/retrofit/blob/master/CHANGELOG.md
timber = "5.0.0" # https://github.com/JakeWharton/timber/blob/master/CHANGELOG.md

# Test libraries
test-junit4 = "4.13.2" # https://github.com/junit-team/junit4/releases
test-junit5 = "5.7.2" # https://github.com/junit-team/junit5/releases
test-androidx-junit = "1.1.3" # https://developer.android.com/jetpack/androidx/releases/test
test-androidx-espresso = "3.4.0" # https://developer.android.com/jetpack/androidx/releases/test
test-robolectric = "4.6.1" # https://github.com/robolectric/robolectric/releases
test-mokitoKotlin = "3.2.0" # https://github.com/mockito/mockito-kotlin/releases
test-jraska-livedata = "1.2.0" # https://github.com/jraska/livedata-testing/releases
test-jakewharton-okhttp3IdlingResource = "1.0.0" # https://github.com/JakeWharton/okhttp-idling-resource/releases

[libraries]
# Kotlin
kotlin-gradle = { module = "org.jetbrains.kotlin:kotlin-gradle-plugin", version.ref = "kotlin" }
kotlinx-coroutines = { module = "org.jetbrains.kotlinx:kotlinx-coroutines-core", version.ref = "kotlinx-coroutines" }

# Android tools
android-gradle = { module = "com.android.tools.build:gradle", version.ref = "androidBuildTools" }

# AndroidX libraries
androidx-core = { module = "androidx.core:core-ktx", version.ref = "androidx-core" }
androidx-coreSplashscreen = { module = "androidx.core:core-splashscreen", version.ref = "androidx-coreSplashscreen" }
andoridx-activityCompose = { module = "androidx.activity:activity-compose", version.ref = "androidx-activity" }
androidx-appcompat = { module = "androidx.appcompat:appcompat", version.ref = "androidx-appcompat" }
# AndroidX lifecycle
androidx-lifecycleLivedata = { module = "androidx.lifecycle:lifecycle-livedata-ktx", version.ref = "androidx-lifecycle" }
androidx-lifecycleViewModel = { module = "androidx.lifecycle:lifecycle-viewmodel-ktx", version.ref = "androidx-lifecycle" }
androidx-lifecycleRuntime = { module = "androidx.lifecycle:lifecycle-runtime-ktx", version.ref = "androidx-lifecycle" }
androidx-lifecycleCommon = { module = "androidx.lifecycle:lifecycle-common-java8", version.ref = "androidx-lifecycle" }
# Androidx navigation
androidx-navigationCompose = { module = "androidx.navigation:navigation-compose", version.ref = "androidx-navigation" }
# Androidx Room
androidx-room = { module = "androidx.room:room-ktx", version.ref = "androidx-room" }
androidx-roomRuntime = { module = "androidx.room:room-runtime", version.ref = "androidx-room" }
androidx-roomCompiler = { module = "androidx.room:room-compiler", version.ref = "androidx-room" }

# Compose
androidx-composeUi = { module = "androidx.compose.ui:ui", version.ref = "androidx-compose" }
androidx-composeUiTooling = { module = "androidx.compose.ui:ui-tooling", version.ref = "androidx-compose" }
androidx-composeFoundation = { module = "androidx.compose.foundation:foundation", version.ref = "androidx-compose" }
androidx-composeMaterial = { module = "androidx.compose.material:material", version.ref = "androidx-compose" }
google-accompanistInsets = { module = "com.google.accompanist:accompanist-insets", version.ref = "google-accompanist" }
google-accompanistAnimatedNavigation = { module = "com.google.accompanist:accompanist-navigation-animation", version.ref = "google-accompanist" }

# Google libraries for Android
google-services = { module = "com.google.gms:google-services", version.ref = "google-services" }
# Material components
google-android-material = { module = "com.google.android.material:material", version.ref = "google-android-material" }

# Halcyon
halcyon-oauthMoshi = { module = "com.halcyonmobile.oauth-setup:oauth-setup-moshi", version.ref = "halcyon-oauth" }
halcyon-oauthStorage = { module = "com.halcyonmobile.oauth-setup:oauth-setup-storage", version.ref = "halcyon-oauth" }
halcyon-errorHandler = { module = "com.halcyonmobile.error-handler:rest", version.ref = "halcyon-errorHandler" }
halcyon-recylerTypedAdapter = { module = "com.halcyonmobile.recycler-extension:typed-adapter", version.ref = "halcyon-recylerTypedAdapter" }

# Other Libraries
# Koin
koin-compose = { module = "io.insert-koin:koin-androidx-compose", version.ref = "koin" }
koin-core = { module = "io.insert-koin:koin-core", version.ref = "koin" }
# LeakCanary
leakcanary = { module = "com.squareup.leakcanary:leakcanary-android", version.ref = "leakcanary" }
# Beagle
beagle-uiDrawer = { module = "com.github.pandulapeter.beagle:ui-drawer", version.ref = "beagle" }
beagle-noop = { module = "com.github.pandulapeter.beagle:noop", version.ref = "beagle" }

# Coil
coil = { module = "io.coil-kt:coil", version.ref = "coil" }
# Square
square-okhttp-loggingInterceptor = { module = "com.squareup.okhttp3:logging-interceptor", version.ref = "square-okhttp" }
square-moshiKotlinCodegen = { module = "com.squareup.moshi:moshi-kotlin-codegen", version.ref = "square-moshi" }
square-moshi = { module = "com.squareup.moshi:moshi", version.ref = "square-moshi" }
square-retrofit = { module = "com.squareup.retrofit2:retrofit", version.ref = "square-retrofit" }
# Timber
timber = { module = "com.jakewharton.timber:timber", version.ref = "timber" }

# Test
test-junit4 = { module = "junit:junit", version.ref = "test-junit4" }
test-junit5-jupiterEngine = { module = "org.junit.jupiter:junit-jupiter-engine", version.ref = "test-junit5" }
test-junit5-jupiterParams = { module = "org.junit.jupiter:junit-jupiter-params", version.ref = "test-junit5" }
test-junit5-vintageEngine = { module = "org.junit.vintage:junit-vintage-engine", version.ref = "test-junit5" }
# Kotlin
test-kotlin-coroutines = { module = "org.jetbrains.kotlinx:kotlinx-coroutines-test", version.ref = "kotlinx-coroutines" }
# AndroidX
test-androidx-core = { module = "androidx.test:core", version.ref = "test-androidx-junit" }
test-androidx-runner = { module = "androidx.test:runner", version.ref = "test-androidx-junit" }
test-androidx-junit = { module = "androidx.test.ext:junit", version.ref = "test-androidx-junit" }
test-androidx-espressoCore = { module = "androidx.test.espresso:espresso-core", version.ref = "test-androidx-espresso" }
test-androidx-espressoIntents = { module = "androidx.test.espresso:espresso-intents", version.ref = "test-androidx-espresso" }
test-androidx-room = { module = "androidx.room:room-testing", version.ref = "androidx-room" }
# Other libraries
test-robolectric = { module = "org.robolectric:robolectric", version.ref = "test-robolectric" }
test-mockitoKotlin = { module = "org.mockito.kotlin:mockito-kotlin", version.ref = "test-mokitoKotlin" }
test-koin = { module = "io.insert-koin:koin-test", version.ref = "koin" }
test-jraska-livedata = { module = "com.jraska.livedata:testing-ktx", version.ref = "test-jraska-livedata" }
test-jakewharton-okhttp3IdlingResource = { module = "com.jakewharton.espresso:okhttp3-idling-resource", version.ref = "test-jakewharton-okhttp3IdlingResource" }
test-square-okhttp-webserver = { module = "com.squareup.okhttp3:mockwebserver", version.ref = "square-okhttp" }

[bundles]
androidx-core = [
    "androidx-core",
    "androidx-coreSplashscreen",
]
androidx-lifecycle = [
    "androidx-lifecycleLivedata",
    "androidx-lifecycleViewModel",
    "androidx-lifecycleRuntime",
    "androidx-lifecycleCommon",
]
androidx-room = [
    "androidx-room",
    "androidx-roomRuntime",
]
androidx-compose = [
    "androidx-composeUi",
    "androidx-composeUiTooling",
    "androidx-composeFoundation",
    "androidx-composeMaterial",
    "google-accompanistInsets",
    "google-accompanistAnimatedNavigation"
]
test-junit5 = [
    "test-junit5-jupiterEngine",
    "test-junit5-jupiterParams",
]
test-robolectric = [
    "test-junit4",
    "test-robolectric",
    "test-androidx-core",
    "test-androidx-runner",
    "test-androidx-junit",
    "test-androidx-espressoCore",
    "test-androidx-espressoIntents",
]
test-android = [
    "test-junit4",
    "test-androidx-core",
    "test-androidx-runner",
    "test-androidx-junit",
    "test-androidx-espressoCore",
    "test-androidx-espressoIntents",
]
